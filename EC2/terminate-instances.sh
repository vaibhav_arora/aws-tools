#!/bin/bash
set -x
while read instanceID
do
	ec2-terminate-instances $instanceID -K pk.pem -C cert.pem 
done < servers.instances
