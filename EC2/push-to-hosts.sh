set -x
nServers=$1
pushedFile=$2

counter=1
for server in $(head -n $nServers servers.hosts)
do
        echo Push to server \#$counter
        scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i sshPrivateKey.pem ${pushedFile} ubuntu@${server}:~
        counter=$((counter+1))
done
