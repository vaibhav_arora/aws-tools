#!/bin/bash
set -x
nservers=$1
rm tmp/servers.instances
ec2-run-instances ami-09aae56c -n $nservers -t m3.xlarge -K pk.pem -C cert.pem -z us-east-1a | awk '{if (NR>1) {print $2}}' >> tmp/servers.instances
