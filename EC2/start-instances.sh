#!/bin/bash
set -x
while read instanceID
do
	ec2-start-instances $instanceID -K pk.pem -C cert.pem 
	ec2-describe-instances $instanceID -K pk.pem -C cert.pem > tmp/description
	cat tmp/description | awk '{if (NR>1) {print $4}}' > servers.hosts
	cat tmp/description | awk '{if (NR>1) {print $13}}' > servers.ips.public
	cat tmp/description | awk '{if (NR>1) {print $14}}' > servers.ips.private
done < servers.instances
