set -x
nServers=$1
pulledFile=$2
pullFromTail=$3

if [ -z "$3" ]
  then
    for server in $(head -n $nServers servers.hosts)
      do 
	scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i sshPrivateKey.pem ubuntu@${server}:~/${pulledFile} ./tmp/
      done
  else
    for server in $(tail -n $nServers servers.hosts)
      do
        scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i sshPrivateKey.pem ubuntu@${server}:~/${pulledFile} ./tmp/
      done
fi

