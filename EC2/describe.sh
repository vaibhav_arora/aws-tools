#!/bin/bash
set -x

rm servers.*
rm tmp/description

if [ -a tmp/servers.instances ]  
then    
  while read instance
    do
      echo $instance " " | tr -d '\n' >> servers.instances    
    done < tmp/servers.instances
    echo >> servers.instances
fi

while read instances
do  
  ec2-describe-instances $instances -K pk.pem -C cert.pem | grep INSTANCE | grep running >> tmp/description
done < servers.instances

cat tmp/description | awk '{print $4}' >> servers.hosts
cat tmp/description | awk '{print $13}' >> servers.ips.public
cat tmp/description | awk '{print $14}' >> servers.ips.private
